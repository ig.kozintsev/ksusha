import Vue from 'vue'
import App from './App.vue'
import router from './router'
import Footer from './components/footer'
import VueProgressiveImage from 'vue-progressive-image'
import { store } from './store/store'

// add footer component
Vue.component('my-footer', Footer);
Vue.use(VueProgressiveImage)
Vue.config.productionTip = false

// add icons
import 'vue-awesome/icons'
import Icon from 'vue-awesome/components/Icon'
Vue.component('icon', Icon)

new Vue({
  store: store,
  el: '#app',
  router,
  template: '<App/>',
  components: { App },
})
