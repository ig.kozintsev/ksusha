import Vue from 'vue';
import Vuex from 'vuex';
import axios from 'axios'

Vue.use(Vuex);

export const store = new Vuex.Store({
  state:{
    genInfo: {},
    domain: 'http://serebrianskaya.com/',
    loadPage: false
  },

  mutations: {
    getGeneralInfo1: function (state, info) {
      state.genInfo = info
    },
    setLoadpage(state, load) {
      state.loadPage = load
    }
  },

  actions: {
      getGeneralInfo: context => {
        let url = context.state.domain + 'authors/1/'
        axios.get(url).then(res => {
          context.commit('getGeneralInfo1', res.data)
        })
      }
  }
})
